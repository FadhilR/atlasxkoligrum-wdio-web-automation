import Page from '../page'
import {assert} from 'chai'

class cartPage extends Page {
    get productDetailPage() {return $('.name')}
    get btnAddtoCart() {return $('.btn-success')}
    get btnNavbarCart() {return $('#cartur')}
    get textProducts() {return $('.col-lg-8 > h2')}
    get btnPlaceOrder() {return $('.btn-success')}
    get txtFieldName() {return $('#name')}
    get txtFieldCountry() {return $('#country')}
    get txtFieldCity() {return $('#city')}
    get txtFieldCreditCard() {return $('#card')}
    get txtFieldMonth() {return $('#month')}
    get txtFieldYear() {return $('#year')}
    get btnPurchase() {return $('[onclick="purchaseOrder()"]')}
    get txtSuccess() {return $('//h2[contains(text(),"Thank you for your purchase!")]')}
    get btnConfirmSuces() {return $('//button[contains(text(),"OK")]')}

    get priceProduct() {return $('.price-container')}

    //cart page
    
    get itemNameColumn(){return $$('#tbodyid td');}
    get itemPriceColumn() {return $$('#tbodyid td');}

    // get alertText() {return browser.getAlertText()}
    
    //Cart page
    async assertItemName(expectItem){
        await browser.pause(2000)
        await this.itemNameColumn[1].waitForDisplayed(2000)
        await expect(await this.itemNameColumn[1]).toHaveText(expectItem)
    }

    async assertItemPrice(expectPrice){
        await browser.pause(2000)
        await this.itemPriceColumn[2].waitForDisplayed(2000)
        await expect(await this.itemPriceColumn[2]).toHaveText(expectPrice)

    }

    async clickBtnPlaceOrder(){
        await this.btnPlaceOrder.waitForDisplayed(2000);
        await this.btnPlaceOrder.click();
    }

    async inputDataOrder(name, country, city, creditC, month, year){
        await this.txtFieldName.waitForDisplayed(2000);
        await this.txtFieldName.setValue(name);
        await this.txtFieldCountry.setValue(country);
        await this.txtFieldCity.setValue(city);
        await this.txtFieldCreditCard.setValue(creditC);
        await this.txtFieldMonth.setValue(month);
        await this.txtFieldYear.setValue(year);
    }

    async clickBtnPurchase(){
        await this.btnPurchase.waitForDisplayed(2000);
        await this.btnPurchase.click();
    }

    async assertSuccessPurchase(){
        // await expect(this.txtSuccessPurchase).toBeDisplayed(2000);
        // await expect(this.txtSuccessPurchase).toHaveText('Thank you for your purchase!');
        // await this.btnConfirmSuces.click();
    }

    open () {
        return super.open('');
    }
}

export default new cartPage();