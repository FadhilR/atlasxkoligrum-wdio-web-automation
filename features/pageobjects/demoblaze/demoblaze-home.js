import Page from '../page'

class homePage extends Page {
    get btnLogout() {return $('[onclick="logOut()"]')}
    get textName() {return $('#nameofuser')}
    // get productHomePage() {return $('#tbodyid > div:nth-of-type(1) .hrefch')}
    // get productCategoryButton() {return $('[onclick="byCat(\'monitor\')"]')}
    productCategoryButton(productCategory) {return $(`[onclick="byCat(\'${productCategory}\')"]`)}
    productHomePage(productName) {return $(`//a[.="${productName}"]`)}

    async assertLogoutLinkText(){
        return await expect(this.btnLogout).toBeDisplayed(2000);
    }
    async assertTextNameUser(){
        await expect(this.textName).toBeDisplayed(2000);
        return await expect(this.textName).toHaveText('Welcome fadhil');
    }
    async clickProduct(productName){
        await this.productHomePage(productName).waitForDisplayed(2000);
        await this.productHomePage(productName).click();
        await browser.pause(2000)
    }
    async clickCatProduct(productCategory){
        await this.productCategoryButton(productCategory).waitForDisplayed(2000);
        await this.productCategoryButton(productCategory).click();
        await browser.pause(2000)
    }

    open () {
        return super.open('');
    }
}

export default new homePage();