import Page from '../page'
import {assert} from 'chai'

class detailPage extends Page {
    get productDetailPage() {return $('.name')}
    get btnAddtoCart() {return $('.btn-success')}
    get btnNavbarCart() {return $('#cartur')}
    get textProducts() {return $('.col-lg-8 > h2')}
    get btnPlaceOrder() {return $('.btn-success')}
    get txtFieldName() {return $('#name')}
    get txtFieldCountry() {return $('#country')}
    get txtFieldCity() {return $('#city')}
    get txtFieldCreditCard() {return $('#card')}
    get txtFieldMonth() {return $('#month')}
    get txtFieldYear() {return $('#year')}
    get btnPurchase() {return $('[onclick="purchaseOrder()"]')}
    get txtSuccess() {return $('//h2[contains(text(),"Thank you for your purchase!")]')}
    get btnConfirmSuces() {return $('//button[contains(text(),"OK")]')}

    get priceProduct() {return $('.price-container')}

    //cart page
    
    get itemNameColumn(){return $$('#tbodyidd td');}
    get itemPriceColumn() {return $$('#tbodyidd td');}

    // get alertText() {return browser.getAlertText()}
    
    async assertDetailProduct(producName){
        await expect(this.productDetailPage).toBeDisplayed(2000);
        return await expect(this.productDetailPage).toHaveText(producName);
    }
    async clickAddtoCartProduct(){
        await this.btnAddtoCart.waitForDisplayed(2000);
        await this.btnAddtoCart.click();
        await browser.pause(2000)
    }
    
    async assertMessageAddToCart(){

        // console.log(this.alertText);
        // return await expect(browser.getAlertText()).toHaveText('Product added.');
        // browser.acceptAlert();
        let actualMessage = await browser.getAlertText();
        console.log(actualMessage)
        await assert.equal(actualMessage,'Product added.');
        await browser.acceptAlert();
    }

    async clickBtnNavbarCart(productName){
        await this.btnNavbarCart.waitForDisplayed(2000);
        await this.btnNavbarCart.click();
        await expect(this.textProducts).toBeDisplayed(2000);
        await expect(this.textProducts).toHaveText(productName);
    }


    //Cart page

    open () {
        return super.open('');
    }
}

export default new detailPage();