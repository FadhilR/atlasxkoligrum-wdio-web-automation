@checkout
#before feature
Feature: add monitor product to cart
    As a user (registered or not) i shouldbe able to add a product

    Background: Login to the web
    #before step
        Given user is on the demoblaze hompage
        #after step
        When user is succesfully tries to logged in
        Then user should see homepage demoblaze
#before scenario
    Scenario: Add product monitor to cart and see detail info
        Given im on <categoryName> page and navigate to <productName> page
        When i add apple monitor 24 to cart
        Then i can see cart detail info <productName> and <productPrice>


    Examples: Example name
            | categoryName | productName      | productPrice |
            | monitor      | Apple monitor 24 | 400          |