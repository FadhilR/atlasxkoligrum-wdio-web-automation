@cartLaptop
#before feature
Feature: add laptop product to cart
    As a user (registered or not) i shouldbe able to add a product

    Background: Login to the web
        #before step
        Given user is on the demoblaze hompage
        #after step
        When user is succesfully tries to logged in
        Then user should see homepage demoblaze
    #before scenario
    Scenario: Add product laptop to cart and see detail info
        Given Im on laptop <categoryName> page and navigate to <productName> page
        When i add Sony vaio i5 to cart
        Then I can see cart detail info laptop <productName> and <productPrice>


        Examples: Example name
            | categoryName | productName  | productPrice |
            | notebook       | Sony vaio i5 | 790          |