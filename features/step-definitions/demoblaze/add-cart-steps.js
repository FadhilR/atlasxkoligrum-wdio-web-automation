import { Given, When, Then } from '@wdio/cucumber-framework';
import homePage from '../../pageobjects/demoblaze/demoblaze-home';
import detailPage from '../../pageobjects/demoblaze/demoblaze-detail';
import cartPage from '../../pageobjects/demoblaze/demoblaze-cart';

Given(/^im on (.+) page and navigate to (.+) page$/, async (categoryName, productName) => {
	await homePage.assertLogoutLinkText();
    await homePage.clickCatProduct(categoryName)
    await homePage.clickProduct(productName)
});

When(/^i add apple monitor 24 to cart$/, async () => {
    await detailPage.assertDetailProduct("Apple monitor 24")
    await detailPage.clickAddtoCartProduct()
    await detailPage.assertMessageAddToCart()
});


Then(/^i can see cart detail info (.+) and (.+)$/, async (productName, productPrice) => {
	await detailPage.clickBtnNavbarCart("Products")
    await cartPage.assertItemName(productName)
    await cartPage.assertItemPrice(productPrice)
});


//step to add product laptop to cart
Given(/^Im on laptop (.+) page and navigate to (.+) page$/, async (categoryName, productName) => {
	await homePage.assertLogoutLinkText();
    await homePage.clickCatProduct(categoryName)
    await homePage.clickProduct(productName)
});
When(/^i add Sony vaio i5 to cart$/, async () => {
	await detailPage.assertDetailProduct("Sony vaio i5")
    await detailPage.clickAddtoCartProduct()
    await detailPage.assertMessageAddToCart()
});

Then(/^I can see cart detail info laptop (.+) and (.+)$/, async (productName, productPrice) => {
	await detailPage.clickBtnNavbarCart("Products")
    await cartPage.assertItemName(productName)
    await cartPage.assertItemPrice(productPrice)
});