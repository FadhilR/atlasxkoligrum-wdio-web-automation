import { Given, When, Then } from '@wdio/cucumber-framework';

import dotenv from 'dotenv';
dotenv.config();

import loginPage from '../../pageobjects/demoblaze/demoblaze-login';

Given(/^user is on the demoblaze hompage$/,async ()=>{
    await loginPage.open();
})

When(/^user is succesfully tries to logged in$/, async ()=>{
    await loginPage.cliclLinkTextLogin();
    await loginPage.inputUsername(process.env.USERNAMEE);
    await loginPage.inputPassword(process.env.PASSWORD);
    await loginPage.clickBtnLogin();
})