import { Given, When, Then } from '@wdio/cucumber-framework';
import detailPage from '../../pageobjects/demoblaze/demoblaze-detail';


When(/^user click button add to cart$/, async ()=>{
    await detailPage.assertDetailProduct()
    await detailPage.clickAddtoCartProduct();
})

Then(/^user should see message success add to cart$/, async ()=>{
    await detailPage.assertMessageAddToCart();
})

Given(/^user is on the demoblaze detail product page$/,async ()=>{
    await detailPage.assertDetailProduct()
})

When(/^user click nvabar button cart$/, async ()=>{
    await detailPage.clickBtnNavbarCart()
})

When(/^user click button place order$/, async ()=>{
    await detailPage.clickBtnPlaceOrder()
})

When(/^user input data order$/, async ()=>{
    await detailPage.inputDataOrder('fadhil', 'indonesia', 'jakarta', '02061212', 'Juli', '1997')
})

When(/^user click button purchase$/, async ()=>{
    await detailPage.clickBtnPurchase()
})

Then(/^user should see message thank you for purchase$/, async ()=>{
    await detailPage.assertSuccessPurchase()
})