import {
    Given,
    When,
    Then
} from '@wdio/cucumber-framework';
import homePage from '../../pageobjects/demoblaze/demoblaze-home';
import detailPage from '../../pageobjects/demoblaze/demoblaze-detail';
import cartPage from '../../pageobjects/demoblaze/demoblaze-cart';


When(/^go to cart page to checkout (.+) and (.+)$/, async (productName, productPrice) => {
    await detailPage.clickBtnNavbarCart("Products")
    await cartPage.assertItemName(productName)
    await cartPage.assertItemPrice(productPrice)
    await cartPage.clickBtnPlaceOrder()
    await cartPage.inputDataOrder('fadhil', 'indonesia', 'jakarta', '02061212', 'Juli', '1997')
    await cartPage.clickBtnPurchase()
});


Then(/^i can see success message pop up$/, async () => {
    await cartPage.assertSuccessPurchase()
});