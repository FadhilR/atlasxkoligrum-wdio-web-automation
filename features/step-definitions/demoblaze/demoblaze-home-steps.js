import { Given, When, Then } from '@wdio/cucumber-framework';
import homePage from '../../pageobjects/demoblaze/demoblaze-home';

Then(/^user should see homepage demoblaze$/, async ()=>{
    await homePage.assertLogoutLinkText();
    await homePage.assertTextNameUser();
})

When(/^user click product samsung s6$/, async ()=>{
    await homePage.clickProduct();
    
})
